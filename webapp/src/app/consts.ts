import { HttpHeaders } from '@angular/common/http';

export class CONSTS {
    _baseUrl = 'http://localhost:8080';
    // _baseUrl = '';
    _headers = {
      headers: new HttpHeaders({'Content-Type':  'application/json'})
    };    
}
