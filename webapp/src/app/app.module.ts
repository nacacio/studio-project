import { AppComponent } from './app.component';
import { AppRoutingModule } from './routes/app.routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';


// CONSTANTES
import { CONSTS } from './consts';

// MODULES
import { MaterialModule } from './modules/material/material.module';
import { LoginModule } from './modules/login/login.module';
import { NgxMaskModule } from 'ngx-mask'

// COMPONENTS
import { NavbarComponent } from './components/navbar/navbar.component';

// SERVICES
import { AuthService } from './services/auth.service';
import { TokenInterceptorService } from './services/token-interceptor.service';

registerLocaleData(ptBr);

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoginModule,
    AppRoutingModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [
    CONSTS, 
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR'
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
