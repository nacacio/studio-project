import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

import { SnackBarCustomService } from './../../services/snack-bar-custom.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private _authService: AuthService,
              private _router: Router,
              private _snackBarCustom:SnackBarCustomService) { }

  ngOnInit() {
  }

  loggedIn(): boolean {
    return this._authService.loggedIn();
  }

  onLogoutClick() {
    this._authService.logout();
    this._router.navigate(['/login']);
    this._snackBarCustom.openMessage('Você Deslogou do Sistema',this._snackBarCustom.snackbar_warning);
    return false;
  }
  onLoginClick() {
    this._authService.logout();
    this._router.navigate(['/login']);
    return false;
  }

}
