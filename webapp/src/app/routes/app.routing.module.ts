import { AuthGuard } from './../guards/auth.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from '../modules/login/components/login/login.component';


const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {
    path: 'client',
    loadChildren: () => import('../modules/client/client.module').then(m => m.ClientModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard]
  },
  {
    path: 'financial',
    loadChildren: () => import('../modules/financial/financial.module').then(m => m.FinancialModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
