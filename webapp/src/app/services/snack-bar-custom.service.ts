import { Injectable } from '@angular/core';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { CONSTS } from './../consts';


@Injectable({
  providedIn: 'root'
})
export class SnackBarCustomService {
  public snackbar_default = 'custom-snack-bar';
  public snackbar_error = 'custom-snack-bar-error';
  public snackbar_sucess = 'custom-snack-bar-sucess';
  public snackbar_warning = 'custom-snack-bar-warning';

  config:MatSnackBarConfig;
  constructor(private _snackBar: MatSnackBar) { 
    this.config = new MatSnackBarConfig();
    this.config.duration = 1500;
    this.config.horizontalPosition = "center";
    this.config.verticalPosition = "top";
  }

  openMessage(msg:string, type:string){
    this.config.panelClass = [this.snackbar_default,type];
    this._snackBar.open(msg,null,this.config);
  }
}
