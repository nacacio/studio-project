import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';

import { CONSTS } from './../consts';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private _injector: Injector, private _consts: CONSTS) { }

  intercept(req, next) {
    if (req.url === this._consts._baseUrl + '/verifytoken' ) {
      const authService = this._injector.get(AuthService);

      const tokenizedReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${authService.getToken()}`
        }
      });
      return next.handle(tokenizedReq);
    } else {
      return next.handle(req);
    }
  }
}
