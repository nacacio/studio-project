import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../modules/user/models/user';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { CONSTS } from './../consts';

@Injectable()
export class AuthService {

  authToken: string;
  user: User;
  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _consts: CONSTS
  ) {
  // VERIFICA TOKEN AO ALTERAR A ROTA
  _router.events.pipe(filter(e => e instanceof NavigationStart)).subscribe(e => {
    const event = e as NavigationStart;
    if (event.url !== '/login'  && event.url !== '/' && event.url !== '/register' &&
      event.url !== '/logout' && event.url !== '/auth') {
       this.verifyTokenServer().subscribe(data => {},
      err => {
        this.logout();
        _router.navigate(['login']);
      });
    }
  });
  }

  // VERIFICA TOKEN LOCAL
  verifyTokenServer() {
    return this._http.get(this._consts._baseUrl + '/verifytoken', this._consts._headers);
  }

  storeUserData(token, user) {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }
  getToken(): string {
    return sessionStorage.getItem('token');
  }
  loggedIn(): boolean {
    return !!this.getToken();
  }
  logout() {
    this.authToken = null;
    this.user = null;
    sessionStorage.clear();
  }
}
