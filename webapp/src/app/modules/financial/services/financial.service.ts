import { FluxoCaixa } from './../models/fluxo-caixa';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CONSTS } from './../../../consts';
@Injectable()
export class FinancialService {

  constructor(private _http: HttpClient,
    private _consts: CONSTS) {}

  createFluxoCaixa(fluxoCaixa: FluxoCaixa) {
  return this._http.post(this._consts._baseUrl + '/fluxo-caixa/create', fluxoCaixa, this._consts._headers);
  }
  createFluxoCaixaArray(fluxosCaixa: FluxoCaixa[]) {
  return this._http.post(this._consts._baseUrl + '/fluxo-caixa/createarray', fluxosCaixa, this._consts._headers);
  }
  readFluxoCaixa() {
  return this._http.get(this._consts._baseUrl + '/fluxo-caixa/read', this._consts._headers);
  }
  updateFluxoCaixa(fluxoCaixa: FluxoCaixa) {
  return this._http.put(this._consts._baseUrl + '/fluxo-caixa/update', fluxoCaixa, this._consts._headers);
  }
  deleteFluxoCaixa(id: string) {
  return this._http.delete(this._consts._baseUrl + '/fluxo-caixa/delete/' + id, this._consts._headers);
  }
  getFluxoCaixaByMonth(mesId: string) {
  return this._http.get(this._consts._baseUrl + '/fluxo-caixa/read/' + mesId, this._consts._headers);
  }
}

