export class FluxoCaixa {
    _id: '';
    tipo_fluxo: '';
    valor: '';
    valor_real: '';
    data_pagamento: '';
    forma_pagamento: '';
    parcelas: '';
    periodo_referencia_inicio: '';
    periodo_referencia_fim: '';
    aluno_referente: '';
    descricao: '';
    observacao: '';
    cpf_pagador: '';
    professor: '';
}
