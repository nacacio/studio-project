import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { FluxoCaixaCreateComponent } from './../components/fluxo-caixa-create/fluxo-caixa-create.component';
import { FluxoCaixaReadComponent } from './../components/fluxo-caixa-read/fluxo-caixa-read.component';


const childRoutes: Routes = [
  {path: 'create', component: FluxoCaixaCreateComponent},
  {path: 'read', component: FluxoCaixaReadComponent},
  {path: '', redirectTo: 'read', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class FinancialRoutingModule { }
