import { PipeModule } from './../pipe/pipe.module';
import { ClientModule } from './../client/client.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancialRoutingModule } from './routes/financial.routing.module';
import { MaterialModule } from './../material/material.module';
import { NgxMaskModule } from 'ngx-mask';

import { SnackBarCustomService } from './../../services/snack-bar-custom.service';
import { FinancialService } from './services/financial.service';

import { FluxoCaixaCreateComponent } from './components/fluxo-caixa-create/fluxo-caixa-create.component';
import { FluxoCaixaReadComponent } from './components/fluxo-caixa-read/fluxo-caixa-read.component';

@NgModule({
  declarations: [FluxoCaixaCreateComponent, FluxoCaixaReadComponent],
  imports: [
    CommonModule,
    FinancialRoutingModule,
    MaterialModule,
    ClientModule,
    PipeModule,
    NgxMaskModule.forChild()
  ],
  providers: [
    FinancialService, 
    SnackBarCustomService
  ]
})
export class FinancialModule { }
