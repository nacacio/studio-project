import { FormGroup, FormBuilder } from '@angular/forms';
import { FinancialService } from './../../services/financial.service';
import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-fluxo-caixa-read',
  templateUrl: './fluxo-caixa-read.component.html',
  styleUrls: ['./fluxo-caixa-read.component.css']
})
export class FluxoCaixaReadComponent implements OnInit, AfterViewInit {
  displayedColumns = ['tipo_fluxo', 'descricao', 'valor','forma_pagamento','periodo_referencia_inicio', 'periodo_referencia_fim', 'professor', 'button'];
  dataSource = new MatTableDataSource();
  formPeriodoReferencia: FormGroup;
  isLoading:boolean = true;
  isEmpty:boolean = true;
  allFluxos;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
      private _router: Router,
      private _snackBarCustom:SnackBarCustomService,
      private _formBuilder: FormBuilder,
      private _financialService:FinancialService) {}

      ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.formPeriodoReferencia = this.createForm();
      }
      ngAfterViewInit() {
        this.getFluxos();     
      }
      getFluxos(){
        let dateAtual = new Date();
        let dateInicio = new Date(dateAtual.getFullYear(),dateAtual.getMonth(),1);
        let dateFim = new Date(dateAtual.getFullYear(),dateAtual.getMonth()+1,0);
        
        this.isLoading = true;
        this.isEmpty = true;
        this._financialService.readFluxoCaixa().subscribe(dados => {
            if (dados['data']?.length > 0) 
            {
              this.allFluxos = dados['data']; 
              this.dataSource.data = this.getFluxoByPeriodo(dateInicio,dateFim);
              this.isEmpty = false;
            }      
            this.isLoading = false;        
        },
        err => {
          this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
          this.isLoading = false;
        });
      }
    
      applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
      }
    
      onRemoveClick(item) {
        this._financialService.deleteFluxoCaixa(item._id).subscribe(dados => {
          if (dados['success']) {
            const index: number = this.dataSource.data.indexOf(item);        
            if (index !== -1) {
              this.dataSource.data.splice(index, 1);
              this.dataSource._updateChangeSubscription();
              this._snackBarCustom.openMessage(dados['msg'],this._snackBarCustom.snackbar_sucess);
            }
          }
        }, err => {
          this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
        });
      }
    
      onEditItem(item) {
        console.log('FUNÇÃO PARA EDITAR ITEM CLICADO:');
        console.log(item); 
      }
      
      getFluxoByPeriodo(dataInicio:Date,dataFim:Date){
        let retorno = [];
        
        this.allFluxos.forEach(fluxo => {
          if(fluxo.periodo_referencia_inicio != undefined && fluxo.periodo_referencia_fim!= undefined)
          { 
            let dateStringInicio = fluxo.periodo_referencia_inicio?.split("/");
            let dateAuxInicio =  new Date(dateStringInicio[2],dateStringInicio[1]-1,dateStringInicio[0]);
            let dateStringFim = fluxo.periodo_referencia_fim?.split("/");
            let dateAuxFim =  new Date(dateStringFim[2],dateStringFim[1]-1,dateStringFim[0]);  
            
            if((dataInicio == null || dateAuxInicio >= dataInicio) && (dataFim == null || dateAuxFim <= dataFim) ){
              retorno.push(fluxo);                      
            }
          }
          else{
            retorno.push(fluxo);
          }
        });
        return retorno;
      }

      createForm():FormGroup {
        let dateAtual = new Date();
        let dateInicio = new Date(dateAtual.getFullYear(),dateAtual.getMonth(),1);
        let dateFim = new Date(dateAtual.getFullYear(),dateAtual.getMonth()+1,0);
        return this._formBuilder.group({
          date_inicio: [{value: dateInicio, disabled:true}],
          date_fim: [{value: dateFim, disabled:true}]
        });
      }

      onSelected(event){
        
        let dataInicioSelected = this.formPeriodoReferencia.value.date_inicio;
        let dataFimSelected = this.formPeriodoReferencia.value.date_fim;

        this.dataSource.data = this.getFluxoByPeriodo(dataInicioSelected,dataFimSelected);        
      }
}
