import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoCaixaReadComponent } from './fluxo-caixa-read.component';

describe('FluxoCaixaReadComponent', () => {
  let component: FluxoCaixaReadComponent;
  let fixture: ComponentFixture<FluxoCaixaReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxoCaixaReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoCaixaReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
