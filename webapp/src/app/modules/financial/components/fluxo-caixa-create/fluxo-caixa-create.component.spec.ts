import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoCaixaCreateComponent } from './fluxo-caixa-create.component';

describe('FluxoCaixaCreateComponent', () => {
  let component: FluxoCaixaCreateComponent;
  let fixture: ComponentFixture<FluxoCaixaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxoCaixaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoCaixaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
