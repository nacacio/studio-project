import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { FinancialService } from './../../services/financial.service';
import { FluxoCaixa } from './../../models/fluxo-caixa';

@Component({
  selector: 'app-fluxo-caixa-create',
  templateUrl: './fluxo-caixa-create.component.html',
  styleUrls: ['./fluxo-caixa-create.component.css']
})
export class FluxoCaixaCreateComponent implements OnInit {
  datePipe: DatePipe = new DatePipe("pt-BR");
  
  formFluxoCaixa: FormGroup;
  fluxoCaixa: FluxoCaixa;

  parcelas = [2, 3, 4, 5, 6];

  tipoFluxos = [
    {id: 1, value: 'Receita'}, 
    {id: 2, value: 'Despesa'}];  

  professores    = [
    {id: 1 , value: 'Amandha da Silva Teixeira'}, 
    {id: 2 , value: 'Claudia Dias da Silva'}];

  formaPagamentos = [
    {id: 1, value: 'Dinheiro'},
    {id: 2, value: 'Cheque'},
    {id: 3, value: 'Transferência Bancária'},
    {id: 4, value: 'Cartão Débito'},
    {id: 5, value: 'Cartão Crédito (à vista)'},
    {id: 6, value: 'Cartão Crédito (parcelado)'}];

  constructor(private _financialService: FinancialService,
              private _formBuilder: FormBuilder,
              private _snackBarCustom: SnackBarCustomService,) {}

  ngOnInit() {
    this.formFluxoCaixa = this.createForm();  
  }

  onSave(){
    if (this.formFluxoCaixa.valid) {
      let fluxosCaixa:FluxoCaixa[] = this.createFluxosByPeriodoReferencia(this.formatFluxoCaixaModel());
      this._financialService.createFluxoCaixaArray(fluxosCaixa).subscribe(
        data => {
          if (data['success']) {          
            this._snackBarCustom.openMessage(data['msg'],this._snackBarCustom.snackbar_sucess);
            this.onNew();      
          }
        },
        err => {
          this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
        }
      ); 
    }
    else{
      this._snackBarCustom.openMessage('Preencha todos os campos obrigatórios',this._snackBarCustom.snackbar_error);
    }  
  }

  onNew(){
    this.formFluxoCaixa.reset({tipo_fluxo: 'Receita',professor: 'Amandha da Silva Teixeira',
    data_pagamento: new Date(),parcelas:{value:'',disabled:true}});
  }

  createFluxosByPeriodoReferencia(fluxoCaixa:FluxoCaixa): FluxoCaixa[]{
    let fluxosReturn:FluxoCaixa[] = [];
    let referenciaInicioAux = fluxoCaixa.periodo_referencia_inicio?.split("/"); 
    let referenciaFimAux = fluxoCaixa.periodo_referencia_fim?.split("/");   
    
    if(referenciaInicioAux!=undefined && referenciaFimAux!=undefined){
      let referenciaInicio:Date = new Date(parseInt(referenciaInicioAux[2]),parseInt(referenciaInicioAux[1])-1,parseInt(referenciaInicioAux[0]));  
      let referenciaFim:Date = new Date(parseInt(referenciaFimAux[2]),parseInt(referenciaFimAux[1])-1,parseInt(referenciaFimAux[0]));           
      let numberOfMonths = this.monthDiff(referenciaInicio,referenciaFim);   
      let valorPerMonth = parseFloat(fluxoCaixa.valor)/(numberOfMonths+1); 
      
      if(numberOfMonths > 0){
        for (let i = 0; i <= numberOfMonths; i++) {
          let fluxoAux = { ...fluxoCaixa };// Pegar os valores sem pegar a referencia 
          fluxoAux.valor = <any>valorPerMonth;
          if(i == 0){ // First Date
            let dateAux = new Date(referenciaInicio.getFullYear(),referenciaInicio.getMonth()+1,0);
            fluxoAux.periodo_referencia_fim = <any>this.datePipe.transform(dateAux,'dd/MM/yyyy');                 
            fluxosReturn.push(fluxoAux);
          }
          else if(i == numberOfMonths){ // Last Date
            let dateAux = new Date(referenciaFim.getFullYear(),referenciaFim.getMonth(),1);
            fluxoAux.periodo_referencia_inicio = <any>this.datePipe.transform(dateAux,'dd/MM/yyyy');
            fluxosReturn.push(fluxoAux);
          }
          else{
            let dateAux = new Date(referenciaInicio.getFullYear(),referenciaInicio.getMonth()+i,1);
            let dateAux2 = new Date(referenciaInicio.getFullYear(),referenciaInicio.getMonth()+i+1,0);
            fluxoAux.periodo_referencia_inicio = <any>this.datePipe.transform(dateAux,'dd/MM/yyyy');
            fluxoAux.periodo_referencia_fim = <any>this.datePipe.transform(dateAux2,'dd/MM/yyyy');
            fluxosReturn.push(fluxoAux);
          }
        }
        return fluxosReturn;
      }
      else{
        fluxosReturn.push(fluxoCaixa);
        return fluxosReturn;
      }
    }
    else{
      fluxosReturn.push(fluxoCaixa);
      return fluxosReturn;
    }
  }

  monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
  }

  formatFluxoCaixaModel(): FluxoCaixa{
    
    var fluxoCaixa = this.formFluxoCaixa.getRawValue();
    var retornoFluxo = new FluxoCaixa();

    retornoFluxo.tipo_fluxo = fluxoCaixa.tipo_fluxo;
    retornoFluxo.valor = fluxoCaixa.valor;
    retornoFluxo.data_pagamento = <any>this.datePipe.transform(fluxoCaixa.data_pagamento,'dd/MM/yyyy');
    retornoFluxo.descricao = fluxoCaixa.descricao?.nome || fluxoCaixa.descricao;
    retornoFluxo.forma_pagamento = fluxoCaixa.forma_pagamento;
    retornoFluxo.parcelas = fluxoCaixa.parcelas.toString();
    retornoFluxo.observacao = fluxoCaixa.observacao;
    
    if(fluxoCaixa.tipo_fluxo == 'Receita'){
      retornoFluxo.professor = fluxoCaixa.professor;
      retornoFluxo.periodo_referencia_inicio = <any>this.datePipe.transform(fluxoCaixa.periodo_referencia_inicio,'dd/MM/yyyy');
      retornoFluxo.periodo_referencia_fim = <any>this.datePipe.transform(fluxoCaixa.periodo_referencia_fim,'dd/MM/yyyy');
      retornoFluxo.aluno_referente = fluxoCaixa.aluno_referente;
      retornoFluxo.cpf_pagador = fluxoCaixa.cpf_pagador;
      retornoFluxo.valor_real = <any>this.getValorReal();
    }
    return retornoFluxo;
  }
  
  getValorReal(){
    return '';
  }

  //Função para criar o Formulario de Fluxo de Caixa
  createForm():FormGroup {
    return this._formBuilder.group({
      tipo_fluxo: ['Receita'], //Entrada ou Saida
      valor: ['', Validators.required],
      valorReal: [''],
      data_pagamento: [new Date()],
      forma_pagamento: [''], //Dinheiro / Cheque / Cartão
      parcelas: [{value:'',disabled:true}, Validators.required],
      periodo_referencia_inicio: [''], 
      periodo_referencia_fim: [''], 
      aluno_referente: [''], // CPF do aluno
      descricao: [''],
      observacao: [''],
      cpf_pagador: [''],
      professor: ['Amandha da Silva Teixeira']
    });
  }
  
  onSelectedFormaPagamento(selected){
    if(selected=='Cartão Crédito (parcelado)'){
      this.formFluxoCaixa.get('parcelas').enable();
    }else{
      this.formFluxoCaixa.get('parcelas').disable();
      this.formFluxoCaixa.get('parcelas').setValue(0);
    }
  }
  autoCompleteOnChange(event){  
    let cpf_aux = event.cpf?.toString().padStart(11,'0')
    if(cpf_aux != undefined){
      this.formFluxoCaixa.get('cpf_pagador').setValue(cpf_aux);
      this.formFluxoCaixa.get('aluno_referente').setValue(cpf_aux);
    }else{
      this.formFluxoCaixa.get('aluno_referente').setValue('');
    }
  }
}
