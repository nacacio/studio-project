import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'telefone'
})
export class TelefonePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if(value?.toString().length == 11)
      return value?.toString().replace(/^(\d{2})(\d{5})(\d{4}).*/, '($1)$2-$3'); // CELULAR
    else
      return value?.toString().replace(/^(\d{2})(\d{4})(\d{4}).*/, '($1)$2-$3'); // FIXO
  }

}
