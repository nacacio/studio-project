import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataNascimento'
})
export class DataNascimentoPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
      return value?.toString().padStart(6,'0').replace(/^(\d{2})(\d{2})(\d{2}).*/, '$1-$2-$3'); // acrescentar 0 no inicio
  }

}
