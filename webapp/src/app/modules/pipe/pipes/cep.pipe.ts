import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cep'
})
export class CepPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return value?.toString().replace(/^(\d{5})(\d{3}).*/, '$1-$2');
  }

}
