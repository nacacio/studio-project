import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconTipo'
})
export class IconTipoPipe implements PipeTransform {

  transform(value: any, args?: any): any {    
    if (value == 'Receita') {
      return 'attach_money';
    } else if (value == 'Despesa') {
      return 'money_off';
    }
    return 'report';
  }

}
