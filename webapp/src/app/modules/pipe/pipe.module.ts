
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CepPipe } from './pipes/cep.pipe';
import { TelefonePipe } from './pipes/telefone.pipe';
import { DataNascimentoPipe } from './pipes/data-nascimento.pipe';
import { CpfPipe } from './pipes/cpf.pipe';
import { IconTipoPipe } from './pipes/icon-tipo.pipe';



@NgModule({
  declarations: [
    CepPipe, 
    TelefonePipe, 
    DataNascimentoPipe, 
    CpfPipe,
    IconTipoPipe

  ],
  exports: [
    CepPipe, 
    TelefonePipe, 
    DataNascimentoPipe,
    CpfPipe,
    IconTipoPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipeModule { }
