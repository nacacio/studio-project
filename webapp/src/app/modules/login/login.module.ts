import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './../material/material.module';
import { UserModule } from './../user/user.module';

import { LoginComponent } from './components/login/login.component';

import { FormValidateService } from './../../utils/form-validate.service';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    UserModule
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    FormValidateService    
  ]
})
export class LoginModule { }
