import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

import { AuthService } from './../../../../services/auth.service';
import { FormValidateService } from './../../../../utils/form-validate.service';
import { UserService } from './../../../user/services/user.service';
import { User } from './../../../user/models/user';
import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  user: User;

  constructor(private _fb: FormBuilder,
              private _authService: AuthService,
              private _userService: UserService,
              private _router: Router,
              private _snackBarCustom:SnackBarCustomService,
              public formValidateService: FormValidateService) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this._fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.user = this.prepareLoginUser();
      this._userService.authUser(this.user).subscribe(
        data => {
          if (data['success']) {
            this._authService.storeUserData(data['token'], data['user']);
            this._router.navigate(['/']);
            this._snackBarCustom.openMessage('Você está Logado',this._snackBarCustom.snackbar_sucess);
          }
        },
        err => {
          this._snackBarCustom.openMessage('Usuário e/ou Senha incorreto(s)',this._snackBarCustom.snackbar_error);
        });
    } else { this.formValidateService.verificaForm(this.loginForm); }
  }

  prepareLoginUser(): User {
    const formModel = this.loginForm.value;

    const loginUser: User = {
      _id: this.user !== undefined ? this.user._id : '',
      username: formModel.username as string,
      password: formModel.password as string
    };
    return loginUser;
  }

  resetForm() {
    this.loginForm.reset();
  }
}
