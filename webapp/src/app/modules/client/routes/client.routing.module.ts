import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ClientReadComponent } from './../components/client-read/client-read.component';
import { ClientCreateComponent } from './../components/client-create/client-create.component';
import { ClientImportComponent } from './../components/client-import/client-import.component';


const childRoutes: Routes = [
  {path: 'create', component: ClientCreateComponent},
  {path: 'update', component: ClientCreateComponent},
  {path: 'read', component: ClientReadComponent},
  {path: 'import', component: ClientImportComponent},
  {path: '', redirectTo: 'read', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class ClientRoutingModule { }
