import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { ClientService } from './../../services/client.service';
import { Client } from './../../models/client';
import { ConsultaCepService } from '../../services/consultacep.service';

import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {
  formClient: FormGroup;
  isLoading:boolean = false;
  isUpdate:boolean = false;

  constructor(private _formBuilder: FormBuilder,
              private _clientService: ClientService,
              private _consultaCepService: ConsultaCepService,
              private _snackBarCustom:SnackBarCustomService,
              private _activatedRoute: ActivatedRoute,
              private _router: Router) {}


  ngOnInit() {
    this.createForm();
    this.getParamsFromURL();
    this.isUpdateView();
  }  

  isUpdateView(){
    this.isUpdate = (this._router.url.split('?')[0] == '/client/update');
  }

  getParamsFromURL(){    
    this._activatedRoute.queryParams.subscribe(params => {
      if(params.clientString != undefined){
        var clientJSON = JSON.parse(params.clientString) || '';
        var client:Client = this._clientService.formatClientFromJSON(clientJSON);
        this.setClientIntoForm(client);
      }
    });
  }
  
  //Click no button Save
  onSubmit() {    
    if (this.formClient.valid) {
      var clientJson = this.formClient.getRawValue();
      var client:Client = this._clientService.formatClientFromJSON(clientJson);
      if (!this.isUpdate) {
        this.addNewClient(client);
      } 
      else {
        this.updateClient(client);
      }
    }    
    else{
      this._snackBarCustom.openMessage('Preencha todos os campos obrigatórios',this._snackBarCustom.snackbar_error);
    }
  }

  updateClient(client:Client){
    this._clientService.updateClient(client).subscribe(
      data => {
        if (data['success']) {          
          this._snackBarCustom.openMessage(data['msg'],this._snackBarCustom.snackbar_sucess);
          this.resetForm();
          this._router.navigate(['client/read']); 
        }
      },
      err => {
        this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
      }
    );
  }

  addNewClient(client:Client){
    this._clientService.createClient(client).subscribe(
      data => {
        if (data['success']) {
          this._snackBarCustom.openMessage(data['msg'],this._snackBarCustom.snackbar_sucess);
          this.resetForm();     
        }
      },
      err => {
        this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
      }
    );
  }

  //Função para criar o Formulario de Cliente
  createForm() {
    this.formClient = this._formBuilder.group({
      nome: ['', Validators.required],
      cpf: ['', Validators.required],
      data_nascimento: [''],
      email: [''],
      telefone: [''],
      endereco: this._formBuilder.group({
          cep: [''],
          logradouro: [{value: '', disabled: true}],
          complemento: [''],
          bairro: [{value: '', disabled: true}],
          localidade: [{value: '', disabled: true}],
          uf: [{value: '', disabled: true}],
          numero: ['']
      })
    });
  }

  resetForm() {
    this.formClient.reset();
    this.patchValueEndereco(''); 
  }

  //ASYNC - AWAIT: configuração para esperar que a chamada no serviço seja realizada por completo.
  async consultaCEP(cep) {   
    var jsonCep:any = await this._consultaCepService.consultaCEPtoJSON(cep);
    this.patchValueEndereco(jsonCep);    
  }
  
  patchValueEndereco(dados) {
    this.formClient.patchValue({
      endereco: {
        cep: dados.cep || '',
        logradouro: dados.logradouro || '',
        bairro: dados.bairro || '',
        localidade: dados.localidade || '',
        uf: dados.uf || ''
      }
    });    
  }

  setClientIntoForm(client:Client) {
    if (client !== undefined) {
      this.formClient.patchValue(client);
    }
  }

  voltarClick(){
    this._router.navigate(['client/read']); 
  }
}
