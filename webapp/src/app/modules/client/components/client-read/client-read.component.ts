import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ClientService } from './../../services/client.service';
import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';

@Component({
  selector: 'app-client-read',
  templateUrl: './client-read.component.html',
  styleUrls: ['./client-read.component.css']
})
export class ClientReadComponent implements AfterViewInit, OnInit {
  displayedColumns = ['cpf', 'nome', 'email', 'data_nascimento', 'telefone', 'cep', 'button'];
  dataSource = new MatTableDataSource();

  isLoading:boolean = true;
  isEmpty:boolean = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
      private _router: Router,
      private _clientService: ClientService, 
      private _snackBarCustom:SnackBarCustomService) {}

  ngOnInit() {
    this.dataSource.filterPredicate = this.filterPredicateCustom;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.getClients();     
  }
  getClients(){
    this.isLoading = true;
    this.isEmpty = true;
    this._clientService.readClient().subscribe(dados => {
        if (dados['data']?.length > 0) 
        {
          this.dataSource.data = dados['data']; 
          this.isEmpty = false;
        }      
        this.isLoading = false;        
    },
    err => {
      this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
      this.isLoading = false;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  onRemoveClick(item) {
    this._clientService.deleteClient(item._id).subscribe(dados => {
      if (dados['success']) {
        const index: number = this.dataSource.data.indexOf(item);        
        if (index !== -1) {
          this.dataSource.data.splice(index, 1);
          this.dataSource._updateChangeSubscription();
          this._snackBarCustom.openMessage(dados['msg'],this._snackBarCustom.snackbar_sucess);
        }
      }
    }, err => {
      this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
    });
  }

  onEditItem(item) {
    const clientString = JSON.stringify(item);
    this._router.navigate(['/client/update'], { queryParams: {clientString} });
  }

  
  filterPredicateCustom (data, filter: string): boolean{ 
    var telefone:string = data.telefone;
    if(telefone.toString().length == 11)
      telefone = telefone.toString().replace(/^(\d{2})(\d{5})(\d{4}).*/, '($1)$2-$3'); // CELULAR
    else
      telefone = telefone.toString().replace(/^(\d{2})(\d{4})(\d{4}).*/, '($1)$2-$3'); // FIXO 

    var camposFilter:string = (data.nome+' '+data.email+' '+telefone+' '+
                              data.cpf?.toString().padStart(11,'0').replace(/^(\d{3})(\d{3})(\d{3})(\d{2}).*/, '$1.$2.$3-$4')+' '+
                              data.data_nascimento?.toString().padStart(6,'0').replace(/^(\d{2})(\d{2})(\d{2}).*/, '$1-$2-$3')+' '+
                              data.endereco?.cep?.toString().replace(/^(\d{5})(\d{3}).*/, '$1-$2'))
                              .trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();   

    var transformedFilter:string = filter.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();

    return camposFilter.indexOf(transformedFilter) != -1;
  }

}
