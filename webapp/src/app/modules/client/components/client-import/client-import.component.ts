import { ConsultaCepService } from './../../services/consultacep.service';
import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import * as XLSX from 'xlsx';

import { ClientService } from './../../services/client.service';
import { Client } from './../../models/client';
import { SnackBarCustomService } from './../../../../services/snack-bar-custom.service';


@Component({
  selector: 'app-client-import',
  templateUrl: './client-import.component.html',
  styleUrls: ['./client-import.component.css']
})
export class ClientImportComponent implements OnInit{

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns = ['cpf', 'nome', 'email', 'data_nascimento', 'telefone', 'cep', 'button'];
  dataSource = new MatTableDataSource();
  filename_default = 'Arquivo selecionado (XLS, XLSX ou CSV)';
  filename = this.filename_default;
  isLoading: boolean = false;
  isEmpty: boolean = true;

  constructor(private _clientService:ClientService, 
              private _snackBarCustom:SnackBarCustomService,
              private _consultaCepService:ConsultaCepService) { }

  ngOnInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }      
  addfile(event)     
  {       
    this.isLoading = true; //loading init    
    var file = event.target.files[0];
    this.filename = file.name;
    var fileReader = new FileReader(); 
    event.target.value = ""; // para conseguir inserir o mesmo arquivo (mesmo nome) novamente sem precisar dar refresh na pagina.
    fileReader.readAsArrayBuffer(file);     
    fileReader.onload = (e) => {    
        var arrayBuffer:any = fileReader.result; 
        var data = new Uint8Array(arrayBuffer);    
        var arr = new Array();    
        for(var i = 0; i != data.length; ++i) 
        {
          arr[i] = String.fromCharCode(data[i]);            
        }
        var bstr = arr.join("");  
        var workbook = XLSX.read(bstr, {type:"binary"});         
        var first_sheet_name = workbook.SheetNames[0];  
        var worksheet = workbook.Sheets[first_sheet_name];

        var customHeader:string[] = ['nome','cpf','data_nascimento','telefone','email','cep','numero','complemento'];
        var clientsJson = XLSX.utils.sheet_to_json(worksheet,{raw:true, header:customHeader,range:1});
        
        //cria array de clientes
        var clientsList:Client[] = [];
        clientsJson.forEach(clientJson => {
          clientsList.push(this._clientService.formatClientFromJSON(<string>clientJson));
        });     
        
        this.upgradeCepJSON(clientsList);
        this.dataSource.data = clientsList;

        if(clientsList.length > 0){
          this.isEmpty = false; 
        }
        
        this.isLoading = false;//loading end
    }    
  }

  saveArrayOfClients(){
    var clientList:Client[] = <Client[]>this.dataSource.data;
    this.isLoading = true; //loading init
    if(this.isClientesImportadosOK(clientList)){
      this._clientService.createClientArray(clientList).subscribe(dados => {
        console.log(dados);
        if (dados['success']) {
          this._snackBarCustom.openMessage('Arquivo importado com sucesso.',this._snackBarCustom.snackbar_sucess);
          this.resetView();
        }
      }, err => {
        this._snackBarCustom.openMessage(err,this._snackBarCustom.snackbar_error);
        this.isLoading = false; //loading end
      }); 
    }
  }

  isClientesImportadosOK(clientList:Client[]){ 
    var isOk:boolean = true;   
    clientList.forEach(client => {
      if(client.cpf == undefined || client.cpf == '' ){        
        this._snackBarCustom.openMessage('Verifique o arquivo importado. (todos clientes devem possuir CPF)',this._snackBarCustom.snackbar_error);
        this.isLoading = false;
        isOk = false;
      }
    });
    return isOk;
  }
  
  upgradeCepJSON(clientList){
    var cepsList:string[] = this.getListCepFromClients(clientList);
    var promisses:Promise<any>[] = [];
    cepsList.forEach(cep => {
        promisses.push(<any>this._consultaCepService.consultaCEPtoJSON(cep));   
    });
    Promise.all(promisses).then(listCeps => {
      var cepAux;
      clientList.forEach(client => {
        cepAux = this.getCepFromArray(listCeps,client.endereco?.cep);
        client.endereco.cep = cepAux?.cep?.toString().replace(/\D/g, '');
        client.endereco.localidade = cepAux?.localidade;
        client.endereco.logradouro = cepAux?.logradouro;
        client.endereco.uf = cepAux?.uf;
        client.endereco.bairro = cepAux?.bairro;
      });
    });
  }

  getCepFromArray(listJson,cep){
    return listJson.find(data => {
      return (data.cep?.toString().replace(/\D/g, '') == cep);
    });
  }

  getListCepFromClients(clientList):string[]{
    var listCeps:string[] = [];
    var cepaux:string = '';
    clientList.forEach(client => {
      cepaux = client.endereco?.cep;
      if(cepaux != undefined && listCeps.indexOf(cepaux) === -1){
        listCeps.push(cepaux);   
      }
    });
    return listCeps;
  }

  resetView(){
    this.dataSource.data = [];
    this.isEmpty = true;
    this.isLoading = false;
    this.filename = this.filename_default;        
  }  

  removeFromImport(item){
    const index = this.dataSource.data.indexOf(item);
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }
}
