import { FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Component, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';

import { ClientService } from './../../services/client.service';
import { Client } from './../../models/client';

@Component({
  selector: 'app-client-autocomplete',
  templateUrl: './client-autocomplete.component.html',
  styleUrls: ['./client-autocomplete.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR, 
      multi: true,
      useExisting: forwardRef(() => ClientAutocompleteComponent)
    }
  ]
})
export class ClientAutocompleteComponent implements OnInit, ControlValueAccessor {

  isSelected = false;
  public clientCtrl: FormControl = new FormControl('');
  filteredClient: Observable<Client[]>;
  clientes: Client[] = [];
  @Output() autoCompleteChange = new EventEmitter<any>();

  private onChage: (obj: any) => void;
  private onTouched: () => void;
  writeValue(obj: any): void{this.clientCtrl.setValue(obj);}
  registerOnChange(fn: any): void{this.onChage = fn;}
  registerOnTouched(fn: any): void{this.onTouched = fn;}
  setDisabledState?(isDisabled:boolean): void{isDisabled ? this.clientCtrl.disable() : this.clientCtrl.enable();}

  constructor(private _clientService: ClientService) {
    this._clientService.readClient().subscribe(data => {
      if (data['success']) {
        this.clientes = data['data'];
      }
    });
  }
  ngOnInit() {
    this.filteredClient = this.clientCtrl.valueChanges
      .pipe(startWith(''),
        map(cliente => this.findOption(cliente))
      );
  }
  
  selectionChange(obj:any){
    this.onChage(obj); 
    this.autoCompleteChange.emit(obj);
    this.isSelected = true;
  }
  doInput(){
    this.onChage(this.clientCtrl.value);
    this.autoCompleteChange.emit(this.clientCtrl.value);
    this.isSelected = false;
  }
  doBlur(){this.onTouched();}

  findOption(val: any) {
    return this.clientes.filter((s) => new RegExp(val, 'gi').test(s.nome));   
  }
  displayFn(cliente: Client) { 
    return cliente ? cliente.nome : undefined;
 }
}
