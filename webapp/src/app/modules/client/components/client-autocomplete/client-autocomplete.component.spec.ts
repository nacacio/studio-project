import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAutocompleteComponent } from './client-autocomplete.component';

describe('ClientAutocompleteComponent', () => {
  let component: ClientAutocompleteComponent;
  let fixture: ComponentFixture<ClientAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
