export class Client {
    _id: '';
    nome: '';
    cpf: '';
    data_nascimento: '';
    email: '';
    telefone: '';
    endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
        uf: '',
        numero: ''
    };
}
