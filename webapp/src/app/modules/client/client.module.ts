import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from '../material/material.module';
import { PipeModule } from './../pipe/pipe.module';
import { NgxMaskModule } from 'ngx-mask';

import { SnackBarCustomService } from './../../services/snack-bar-custom.service';

import { ClientRoutingModule } from './routes/client.routing.module';
import { ClientAutocompleteComponent } from './components/client-autocomplete/client-autocomplete.component';
import { ClientReadComponent } from './components/client-read/client-read.component';
import { ClientCreateComponent } from './components/client-create/client-create.component';
import { ClientService } from './services/client.service';
import { ClientImportComponent } from './components/client-import/client-import.component';
import { ConsultaCepService } from './services/consultacep.service';



@NgModule({
  declarations: [
    ClientAutocompleteComponent,
    ClientCreateComponent,
    ClientReadComponent,
    ClientImportComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    ClientRoutingModule,
    PipeModule,
    NgxMaskModule.forChild()
  ],
  exports: [
    ClientAutocompleteComponent
  ],
  providers: [
    ClientService, 
    ConsultaCepService,
    SnackBarCustomService
  ]
})
export class ClientModule { }
