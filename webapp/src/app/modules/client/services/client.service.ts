import { Client } from './../models/client';
import { CONSTS } from './../../../consts';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ClientService {
  constructor(private _http: HttpClient,
              private _consts: CONSTS) {}

  createClient(cliente: Client) {
  return this._http.post(this._consts._baseUrl + '/cliente/create', cliente, this._consts._headers);
  }
  readClient() {
  return this._http.get(this._consts._baseUrl + '/cliente/read', this._consts._headers);
  }
  updateClient(cliente: Client) {
  return this._http.put(this._consts._baseUrl + '/cliente/update', cliente, this._consts._headers);
  }
  deleteClient(id: string) {
  return this._http.delete(this._consts._baseUrl + '/cliente/delete/' + id, this._consts._headers);
  }
  createClientArray(clientes: Client[]) {
  return this._http.post(this._consts._baseUrl + '/cliente/createarray',clientes, this._consts._headers);
  }
  
  formatClientFromJSON(clientJson): Client{
    var client:Client = new Client();

    client.cpf = clientJson.cpf?.toString().replace(/\D/g, ''); // Para deixar apenas numeros
    client.nome = clientJson.nome || '';
    client.email = clientJson.email || '';
    client.data_nascimento = clientJson.data_nascimento?.toString().replace(/\D/g, '') || '';
    client.telefone = clientJson.telefone?.toString().replace(/\D/g, '') || '';
    client.endereco = {
          cep: clientJson.cep?.toString().replace(/\D/g, '') || clientJson.endereco?.cep.toString().replace(/\D/g, '') || '',
          logradouro: clientJson.logradouro|| clientJson.endereco?.logradouro || '',
          complemento: clientJson.complemento|| clientJson.endereco?.complemento || '',
          bairro: clientJson.bairro|| clientJson.endereco?.bairro || '',
          localidade: clientJson.localidade || clientJson.endereco?.localidade || '',
          uf: clientJson.uf || clientJson.endereco?.uf|| '',
          numero: clientJson.numero || clientJson.endereco?.numero || ''
    };

    return client;
  }
  
}
