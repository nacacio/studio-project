import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ConsultaCepService {

  constructor(private http: HttpClient) { }
  // Expressão regular para validar o CEP.
  validacep:RegExp = /^[0-9]{8}$/;
    
  consultaCEPtoJSON(cep){  
    if (this.validacep.test(cep?.toString().replace(/\D/g, ''))) {
      return this.http.get(`//viacep.com.br/ws/${cep}/json`).toPromise();
    }
    return {erro: true}; 
  } 

  }
