import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './../material/material.module';

// Service
import { UserService } from './services/user.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule
  ],
  declarations: [],
  exports: [],
  providers: [
    UserService    
  ]
})
export class UserModule { }
