import { CONSTS } from './../../../consts';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  constructor(private _http: HttpClient,
              private _consts: CONSTS) {}

  createUser(usuario) {
  return this._http.post(this._consts._baseUrl + '/usuario/create', usuario, this._consts._headers);
  }
  readUser() {
  return this._http.get(this._consts._baseUrl + '/usuario/read', this._consts._headers);
  }
  updateUser(usuario) {
  return this._http.put(this._consts._baseUrl + '/usuario/update', usuario, this._consts._headers);
  }
  deleteUser(id: string) {
  return this._http.delete(this._consts._baseUrl + '/usuario/delete/' + id, this._consts._headers);
  }
  authUser(usuario) {
  return this._http.post(this._consts._baseUrl + '/usuario/auth', usuario, this._consts._headers);
  }
}
