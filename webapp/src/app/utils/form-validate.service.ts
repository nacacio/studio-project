import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class FormValidateService {

  constructor() { }

  verificaForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(campo => {
      const controle = formGroup.get(campo);
      controle.markAsDirty();
      controle.markAsTouched();
      if (controle instanceof FormGroup) {
        this.verificaForm(controle);
      }
    });
  }

  getMsg(campo: string, formGroup: FormGroup): string {
    const controle = formGroup.get(campo);
    return controle.hasError('required') ? 'O Campo deve ser preenchido' :
    controle.hasError('email') ? 'Email inválido' : 'Erro';
  }

  isValid(campo: string, formGroup: FormGroup): boolean {
    return  formGroup.get(campo).valid;
  }
}
