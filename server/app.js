const express = require('express');
const router = express.Router();
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/database');
const jwt = require('jsonwebtoken');
const moment = require('moment');
moment.locale('pt-BR');

const app = express();

//Numero da porta do servidor
const port = process.env.PORT || 8080;

//Conecta a mongoDB
mongoose.connect(config.database,{ useNewUrlParser: true, useUnifiedTopology: true });

// On Connection
mongoose.connection.on('connected', () => {
  console.log('#### Banco de Dados conectado '+config.database);
});
// On Error
mongoose.connection.on('error', (err) => {
  console.log('#### Erro no Banco de Dados '+err);
});

mongoose.set('useFindAndModify', false);

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());


const usuariosRoutes = require('./routes/usuarios');
const clientesRoutes = require('./routes/clientes');
const fluxoCaixaRoutes = require('./routes/fluxo-caixa');

app.use('/usuario', usuariosRoutes);
app.use('/cliente', clientesRoutes);
app.use('/fluxo-caixa', fluxoCaixaRoutes);

// Start Server
app.listen(port, () => {
  console.log('#### Server iniciado na porta '+port);
});

// ROTA DE VERIFICAÇÃO DE TOKEN
app.get('/verifytoken',(req, res) => {
  if(req.headers.authorization == undefined)
  { return res.status(401).send('Requisição não autorizada - Sem Authorization')}
  var token = req.headers.authorization.split(' ')[1];
  if(token == undefined)
  { return res.status(401).send('Requisição não autorizada - Sem Token')}
  //VERIFICA TOKEN
  jwt.verify(token,config.secret,function(err, decoded) {
      if (err){res.status(401).send('Requisição não autorizada - Token Inválido')}
      else {req.userId = decoded.subject;
            return res.status(200).json({success: true, msg: 'OK'});}
  });
});
