const express = require('express');
const router = express.Router();
const FluxoCaixa = require('../models/fluxo-caixa');
const { Schema } = require('mongoose');
//CRUD
router.post('/create', (req,res,next) =>{
  FluxoCaixa.create(req.body,(err,fluxoCaixa) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: fluxoCaixa, msg:'fluxo de caixa criado com sucesso'});}
  })
});
router.get('/read', (req,res,next) =>{
  FluxoCaixa.find({},(err,fluxosCaixa) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: fluxosCaixa, msg:'Lista de fluxos de caixa recuperada com sucesso'});}
  }).populate('usuario');
});
router.put('/update', (req,res,next) =>{
  FluxoCaixa.findOneAndUpdate({_id:req.params.id},req.body,(err,fluxoCaixa) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{
      res.status(200).json({success: true, data: fluxoCaixa, msg:'fluxo de caixa atualizado com sucesso'});}
  })
});
router.delete('/delete/:id', (req,res,next) =>{
  FluxoCaixa.findOneAndRemove({_id:req.params.id},(err,fluxoCaixa)=>{
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: fluxoCaixa, msg:'fluxo de caixa deletado com sucesso'});}
  });
});

router.post('/createarray', (req,res,next) =>{
  FluxoCaixa.collection.insertMany(req.body)
  .then( fluxo => {
    res.status(200).json({success: true, data: fluxo, msg:'Fluxo(s) de Caixa adicionado(s) com sucesso'});
  })
  .catch( err => {
    res.status(500).json({success: false, data: err});
  });
});

module.exports = router;
