const express = require('express');
const router = express.Router();
const Cliente = require('../models/cliente');
const { Schema } = require('mongoose');

//CRUD
router.post('/create', (req,res,next) =>{
  Cliente.create(req.body,(err,cliente) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: cliente, msg:'Cliente criado com sucesso'});}
  })
});
router.get('/read', (req,res,next) =>{
  Cliente.find({},(err,clientes) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: clientes, msg:'Lista de clientes recuperada com sucesso'});}
  }).populate('usuario');
});
router.put('/update', (req,res,next) =>{
  Cliente.findOneAndUpdate({ cpf: req.body.cpf },req.body,(err,cliente) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{
      res.status(200).json({success: true, data: cliente, msg:'Cliente atualizado com sucesso'});}
  })
});
router.delete('/delete/:id', (req,res,next) =>{
  Cliente.findOneAndRemove({_id:req.params.id},(err,cliente)=>{
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: cliente, msg:'Cliente deletado com sucesso'});}
  });
});

router.post('/createarray', (req,res,next) =>{
  var bulkOps = [];
  req.body.forEach(client => {
    let upsertDoc = {
      'updateOne': {
        'filter': { 'cpf': client.cpf},
        'update': { '$set': client },
        'upsert': true
      }};
    bulkOps.push(upsertDoc);
  });
  Cliente.collection.bulkWrite(bulkOps)
  .then( cliente => {
    res.status(200).json({success: true, data: cliente, msg:'Clientes adicionados com sucesso'});
  })
  .catch( err => {
    res.status(500).json({success: false, data: err});
  });
});

module.exports = router;
