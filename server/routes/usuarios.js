const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Usuario = require('../models/usuario');


//CRUD
router.post('/create', (req,res,next) =>{
  var novoUsuario = createUsuario(req.body);
  novoUsuario.save((err,novoUsuario) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: novoUsuario, msg:'Usuário criado com sucesso'});}
  })
});
router.get('/read', (req,res,next) =>{
  Usuario.find({},(err,usuarios) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: usuarios, msg:'Lista de usuários recuperada com sucesso'});}
  })
});
router.put('/update', (req,res,next) =>{
  var usuario = createUsuario(req.body);
  Usuario.findByIdAndUpdate(req.body._id,usuario,(err,usuario) => {
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: usuario, msg:'Usuário alterado com sucesso'});}
  })
});
router.delete('/delete/:id', (req,res,next) =>{
  Usuario.findOneAndRemove({_id:req.params.id},(err,usuario)=>{
    if(err){res.status(500).json({success: false, data: err});}
    else{res.status(200).json({success: true, data: usuario, msg:'Usuário removido com sucesso'});}
  });
});

// Authenticate
router.post('/auth', (req, res, next) => {
  let username = req.body.username;
  let password = req.body.password;
  let isMatch = false;


  Usuario.findOne({"username":username},(err,usuario) => {
    if(err){res.status(500).json({success: false, data: err});}
    else if(!usuario){res.status(500).json({success: false, msg: 'Usuário não encontrado'});}
    else{
      isMatch = false;
      if(password != undefined && usuario.password != undefined){
        isMatch = bcrypt.compareSync(password, usuario.password);
      }
      if(isMatch){
        res.status(200).json({
          success: true,
          token: jwt.sign({subject: usuario._id},config.secret, {
            expiresIn: 604800 // 1 Semana
          }),
          user: {
            id: usuario._id,
            username: usuario.username
          },
          msg:"Você está connectado"
        })
      }
      else {
        return res.status(500).json({success: false, msg: 'Senha incorreta, favor tentar novamente'});
      }
    }
  });
});

//FUNC AUXILIAR
function createUsuario(usuario){
  return new Usuario ({
    username: usuario.username,
    password: bcrypt.hashSync(usuario.password, 10)
  });
}
module.exports = router;
