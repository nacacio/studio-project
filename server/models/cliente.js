const mongoose = require('mongoose');
const ClienteSchema = mongoose.Schema ({
  nome: {
    type: String,
    required: true
  },
  cpf: {
    type: String
  },
  data_nascimento: {
    type: String
  },
  email: {
    type: String
  },
  telefone: {
    type: String
  },
  endereco: {
    cep: {
      type: String
    },
    logradouro: {
      type: String
    },
    bairro: {
      type: String
    },
    localidade: {
      type: String
    },
    uf: {
      type: String
    },
    numero: {
      type: String
    },
    complemento: {
      type: String
    }
  }
});
module.exports = mongoose.model('Cliente', ClienteSchema);
