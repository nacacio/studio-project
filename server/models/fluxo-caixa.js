const mongoose = require('mongoose');
const FluxoCaixaSchema = mongoose.Schema ({
  tipo_fluxo: {
    type: String,
    required: true
  },
  valor: {
    type: String
  },
  valor_real: {
    type: String
  },
  data_pagamento: {
    type: String
  },
  forma_pagamento: {
    type: String
  },
  parcelas: {
    type: String
  },
  periodo_referencia_inicio: {
    type: String
  },
  periodo_referencia_fim: {
    type: String
  },
  aluno_referente: {
    type: String
  },
  descricao: {
    type: String
  },
  observacao: {
    type: String
  },
  cpf_pagador: {
    type: String
  },
  professor: {
    type: String
  },
});

module.exports = mongoose.model('FluxoCaixa', FluxoCaixaSchema);
