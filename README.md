# StudioProject

## Para Instalar:
- Realizar o clone do repository
- Realizar npm install (para instalar as libs do package)
- O mesmo para o Servidor Node.js

## Para Executar
- Navegar até a pasta do app
- Executar no terminal : ng serve

## Para Executar o servidor Node.js
- Navegar até a pasta do servidor
- Caso queira utilizar como livereload, Instalar o nodemon
- Executar no terminal : nodemon app.js

# Lista de Tarefas
## Module Client
- [x] Criar CRUD Client
- [ ] Aplicar Locale no componente

### Client Import
- [x] Criar Tela de Import
- [x] Utilizar a [XLSX](https://github.com/SheetJS/sheetjs) para importar planilhas 
- [x] Adicionar colunas ao DataGrid - Numero e Complemento
- [x] Não deixar inserir Cliente sem CPF
- [x] Buscar CEP utilizando o [ViaCep](https://viacep.com.br/) e atualizar o endereço do "Cliente" 
- [ ] Sort do Datagrid Ignorando Acentos/Caracteres Especiais
- [ ] Corrigir entrada por CSV
- [ ] Adicionar botão de ajuda abrindo um Dialog com imagem de planilha de exemplo

### Client Read
- [x] Criar Tela de Read
- [x] Criar função de Busca de Clientes
- [x] Criar Função de Editar ao dar DoubleClick na Linha
- [x] Filter Custom do Datagrid
- [x] Adicionar Refress Button
- [x] Adicionar Botao de Voltar no Componente de Create Cliente
- [ ] Sort do Datagrid Ignorando Acentos/Caracteres Especiais
- [ ] Adicionar Dialog de Confirmação (para remoção da linha)
- [ ] Deshabilitar button de salvar caso não haja alteração.

### Client Create
- [x] Criar Tela de novo Cliente
- [x] Buscar CEP utilizando o [ViaCep](https://viacep.com.br/) para atualizar as informações
- [x] Adicionar Botão de Novo para resetar o form
- [x] Aplicar Mask aos itens do form (CEP, CPF, DATA, TELEFONE)
- [x] Arrumar Layout da Tela
- [x] Refatorar códigos
